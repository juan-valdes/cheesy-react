import React from "react";
import "./CheeseForm.css";

const cheeseForm = (props) => {
    const addDisabled = !(props.newCheese.name && props.newCheese.tasteId);
    return (
        <div className="newcheese needs-validation">
            <h4 className="text-center mb-3">New Cheese</h4>
            <div className="form-group row">
                <label className="col-sm-3 col-form-label"
                    htmlFor="cheese-input">Cheese
                </label>
                <div className="col-sm-9">
                    <input id="cheese-input"
                        type="text"
                        className="form-control"
                        placeholder="Enter Cheese..."
                        defaultValue={props.cheeseDefaultValue}
                        required=""
                        disabled={props.disabled || ""}
                        onChange={props.nameHandler}/>
                    <small className="text-muted">What cheese are you thinking?</small>
                </div>
            </div>
            <div className="form-group row">
                <label className="col-sm-3 col-form-label"
                htmlFor="taste-select">Taste</label>
                <div className="col-sm-9">
                    <select id="taste-select"
                        className="form-control"
                        defaultValue={props.tasteDefaultValue}
                        required=""
                        onChange={props.optionHandler}>
                        <option defaultValue key={0}/>
                        {props.tastes.map(option =>
                            <option key={option.id}>
                            {option.name}
                            </option>
                        )}
                    </select>
                    <small className="text-muted">Select the closest option</small>
                </div>
            </div>
            <div className="text-right">
                <button type="submit" className="btn btn-primary" onClick={props.addCheeseHandler} disabled={addDisabled}>Add</button>
            </div>
        </div>
    );
};

export default cheeseForm;