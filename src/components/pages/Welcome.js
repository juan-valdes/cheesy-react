import React from "react";
// import "./Welcome.css";

const welcome = (props) => {
    return (
        // <div className={"Welcome"}>
        //     Welcome!
        // </div>
        <main role="main" className="container">
            <div className="starter-template">
                {props.content}
            </div>
        </main>
    );
};

export default welcome;