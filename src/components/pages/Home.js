import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withAuth } from "@okta/okta-react";
import Welcome from "./Welcome";

export default withAuth(class Home extends Component {
    state = { authenticated: null }

    checkAuthentication = async () => {
        const authenticated = await this.props.auth.isAuthenticated();
            if (authenticated !== this.state.authenticated) {
            this.setState({ authenticated });
        }
    }

    async componentDidMount() {
        this.checkAuthentication();
    }

    async componentDidUpdate() {
        this.checkAuthentication();
    }

    login = async () => {
        this.props.auth.login("/");
    }

    logout = async () => {
        this.props.auth.logout("/");
    }

    render() {
        if (this.state.authenticated === null) return null;
        const welcomeContent = this.state.authenticated ?
            (
                <div>
                    <p className="lead">
                        You are logged in, you can go to the Console and create some Cheese
                    {/* Protected Route */}
                    </p>
                    <Link className="btn btn-primary btn-lg active" to="/console">Console</Link>
                </div>
            ) : (
                <div>
                    <p className="lead">
                        If you are a Cheesy Staff Member, please get your credentials from your
                        supervisor
                    </p>
                    <button className="btn btn-success btn-lg active" onClick={this.login}>Login</button>
                </div>
            );

        return (
            <Welcome content={welcomeContent}/>
        );
    }
});