import React, { Component } from "react";

import CheesesTable from "../CheesesTable/CheesesTable";
import EditModal from "../Modals/EditModal";
import DeleteModal from "../Modals/DeleteModal";
import CheeseForm from "../CheeseForm/CheeseForm";
import 'react-table/react-table.css';
import "./CheesyConsole.css";

class CheesyConsole extends Component {
  state = {
    users: [
      {id: 1, username: "user1", password: 1234, enabled: true},
      {id: 2, username: "user2", password: 1234, enabled: true}
    ],
    userRoles: [
      {userId: 1, role: "USER"},
      {userId: 2, role: "VIEW ONLY"}
    ],
    cheeses: [
      {id: 1, name: "Gouda", tasteId: 3},
      {id: 2, name: "Cheddar", tasteId: 4},
      {id: 3, name: "Bleu", tasteId: 1}
    ],
    tastes: [
      {id: 1, name: "Horrible"},
      {id: 2, name: "So-So"},
      {id: 3, name: "Good"},
      {id: 4, name: "Delicious"}
    ],
    newCheese: {name: null, tasteId: null},
    editModal: null,
    deleteModal: null,
  };

  addCheeseHandler = () => {
    const {name, tasteId} = {...this.state.newCheese};
    if(name && tasteId){
      const cheesesIds = this.state.cheeses.map(ch =>
        ch.id );
        const cheeses = [...this.state.cheeses];
        cheeses.push({id: Math.max(...cheesesIds) + 1,
          name,
          tasteId
        });
        this.setState({cheeses});
    }
  }
  editCheeseHandler = (cheeseId) => {
    const {name, tasteId} = {...this.state.newCheese};
    if(name || tasteId){
        const cheeses = [...this.state.cheeses];
        const index = cheeses.findIndex(cheese => {
          return cheese.id === cheeseId;
        });
        cheeses[index].name = name || cheeses[index].name;
        cheeses[index].tasteId = tasteId || cheeses[index].tasteId;
        this.setState({cheeses});
    }
  }

  optionHandler = (event) => {
    const newCheese = {...this.state.newCheese};
    newCheese.tasteId = null;
    if(event.target.value){
    const taste = this.state.tastes.find(t =>
        t.name === event.target.value);
    newCheese.tasteId = taste.id || null;
    }
    this.setState({newCheese});
  }

  nameHandler = (event) => {
    const newCheese = {...this.state.newCheese};
    newCheese.name = event.target.value;
    this.setState({newCheese});

  }

  deleteHandler = (cheeseId) => {
    const index = this.state.cheeses.findIndex(cheese => cheese.id === cheeseId)
    const cheeses = [...this.state.cheeses];
    cheeses.splice(index, 1);
    this.setState({cheeses});
  }

  createEditModalHandler = (cheeseId) => {
    const cheese = this.state.cheeses.find(cheese => cheese.id === cheeseId);
    const taste = this.state.tastes.find(taste => taste.id === cheese.tasteId);
    this.setState({editModal: <EditModal
        key={"edit-modal-" + cheese.id}
        dataTarget={"edit-" + cheese.id}
        name={cheese.name}
        editCheeseHandler={() => this.editCheeseHandler(cheese.id)}
        tastes={this.state.tastes}
        // disabled={"disabled"}
        cheeseDefaultValue={cheese.name}
        tasteDefaultValue={taste.name}
        nameHandler={this.nameHandler}
        optionHandler={this.optionHandler}
        />});
  }

  createDeleteModalHandler = (cheeseId) => {
    const cheese = this.state.cheeses.find(cheese => cheese.id === cheeseId);
    this.setState({deleteModal: <DeleteModal
        key={"delete-modal-" + cheese.id}
        dataTarget={"delete-" + cheese.id}
        name={cheese.name}
        deleteHandler={() => this.deleteHandler(cheese.id)}/>});
  }

  render() {
    return (
      <div className="CheesyConsole">
        <div className="title mx-auto">
            <h1>Cheesy Console</h1>
        </div>
        <CheeseForm
          tastes={this.state.tastes}
          disabled = ""
          nameHandler={this.nameHandler}
          optionHandler={this.optionHandler}
          addCheeseHandler={this.addCheeseHandler}
          newCheese={this.state.newCheese}
        />

        <hr className="mb-4"/>

        <CheesesTable
          cheeses={this.state.cheeses}
          tastes={this.state.tastes}
          onMouseEnterEdit={this.createEditModalHandler}
          onMouseEnterDelete={this.createDeleteModalHandler}
        />
          {this.state.editModal}
          {this.state.deleteModal}
        </div>
    );
  }
}

export default CheesyConsole;
