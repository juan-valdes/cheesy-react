import React from "react";
import "./Modals.css";

const deleteModal = (props) => {
    return (
        <div className="Modals modal fade" id={props.dataTarget} role="dialog">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header d-inline">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        <h4 className="modal-title">Delete Confirmation</h4>
                    </div>
                    <div className="modal-body">
                        <p>Do you realy want to delete the <strong>{props.name}</strong> cheese?</p>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" className="btn btn-danger" data-dismiss="modal" onClick={props.deleteHandler}>Delete</button>
                    </div>
                </div>
            </div>
        </div>
        );
};

export default deleteModal;