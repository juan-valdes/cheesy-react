import React from "react";
import "./Modals.css";

const editModal = (props) => {
    return (
        <div className="Modals modal fade" id={props.dataTarget} role="dialog">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header d-inline">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        <h4 className="modal-title">Edit Confirmation</h4>
                    </div>
                    <div className="modal-body">
                        <p>Do you realy want to edit the <strong>{props.name}</strong> cheese?</p>
                        <div className="newcheese needs-validation">
                            <h4 className="text-center mb-3">New Cheese</h4>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label"
                                    htmlFor="cheese-input">Cheese
                                </label>
                                <div className="col-sm-10">
                                    <input id="cheese-input"
                                        type="text"
                                        className="form-control"
                                        placeholder="Enter Cheese..."
                                        defaultValue={props.cheeseDefaultValue}
                                        required=""
                                        disabled={props.disabled || ""}
                                        onChange={props.nameHandler}/>
                                    <small className="text-muted">What cheese are you thinking?</small>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label"
                                htmlFor="taste-select">Taste</label>
                                <div className="col-sm-10">
                                    <select id="taste-select"
                                        className="form-control"
                                        defaultValue={props.tasteDefaultValue}
                                        required=""
                                        onChange={props.optionHandler}>
                                        <option defaultValue key={0}/>
                                        {props.tastes.map(option =>
                                            <option key={option.id}>
                                            {option.name}
                                            </option>
                                        )}
                                    </select>
                                    <small className="text-muted">Select the closest option</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={props.editCheeseHandler}>Save</button>
                    </div>
                </div>
            </div>
        </div>
        );
};

export default editModal;