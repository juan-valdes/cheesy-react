import React from "react";
import ReactTable from 'react-table';

const cheesesTable = (props) => {
    const columns = [{
        Header: 'ID',
        accessor: 'id',
        style: {
          textAlign: "center"
        },
        // width: "auto",
        // maxWidth: 100,
        minWidth:20,
      }
      , {
        Header: 'Cheese',
        accessor: 'name',
        style: {
          textAlign: "left"
        },
        // width: "auto",
        // maxWidth: 100,
        minWidth: 100,
      }, {
        Header: 'Taste',
        id: 'tasteId',
        accessor: cheese => props.tastes[cheese.tasteId - 1].name,
        style: {
          textAlign: "left"
        },
      }, {
        Header: "Edit",
        id: "id",
        // accessor: cheese => cheese.id,
        accessor: "id",
        style: {
          textAlign: "center"
        },
        Cell: cellProps => <button
            type="button"
            className="btn btn-success"
            data-toggle="modal"
            data-target={"#edit-" + cellProps.original.id}
            onMouseEnter={() => props.onMouseEnterEdit(cellProps.original.id)}
            >
            Edit
          </button>,
        sortable: false,
        filterable: false,
        width: 120,
      }, {
        Header: "Delete",
        id: "id",
        style: {
          textAlign: "center"
        },
        // accessor: cheese => cheese.id,
        accessor: "id",
        Cell: cellProps => <button
            type="button"
            className="btn btn-danger"
            data-toggle="modal"
            data-target={"#delete-" + cellProps.original.id}
            onMouseEnter={() => props.onMouseEnterDelete(cellProps.original.id)}
            >
            Delete
          </button>,
        sortable: false,
        filterable: false,
        width: 120,
      }
    ];
    return (
        <div className="container">
          <ReactTable
            data={props.cheeses}
            columns={columns}
            filterable
            defaultPageSize={5}
            // pageSizeOptions
            showPagination={false}
            noDataText = "Please wait..."
            />
        </div>
    );
};

export default cheesesTable;