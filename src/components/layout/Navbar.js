import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withAuth } from "@okta/okta-react";

export default withAuth(class Navbar extends Component {
    state = { authenticated: null }

    checkAuthentication = async () => {
        const authenticated = await this.props.auth.isAuthenticated();
            if (authenticated !== this.state.authenticated) {
            this.setState({ authenticated });
        }
    }

    async componentDidMount() {
        this.checkAuthentication();
    }

    async componentDidUpdate() {
        this.checkAuthentication();
    }

    login = async () => {
        this.props.auth.login("/");
    }

    logout = async () => {
        this.props.auth.logout("/");
    }

    render(){
        const navStyle = {cursor: "pointer"};
        const consoleNav = this.state.authenticated ?
            (<Link className="nav-link" to="/console">
                Console
            </Link>)
            : null;
        const loginNav = this.state.authenticated ?
            (<p className="btn btn-secondary active"
                onClick={this.logout}
                style={navStyle}>Logout</p>)
            :
            (<p className="btn btn-success active"
                onClick={this.login}
                style={navStyle}>Login</p>);

        return(
            <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                <div className="container">
                    <Link className="navbar-brand" to="/">
                        Cheesy React App
                    </Link>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarNav"
                    >
                    <span className="navbar-toggler-icon"/>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/">
                                    Home
                                </Link>
                            </li>
                            <li className="nav-item">
                                {consoleNav}
                            </li>
                            <li className="nav-item">
                                {loginNav}
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }

});

// export default Navbar;