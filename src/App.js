import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Security, SecureRoute, ImplicitCallback } from "@okta/okta-react";

import Navbar from "./components/layout/Navbar";
import Home from "./components/pages/Home";
import Login from "./components/auth/Login";

import CheesyConsole from "./components/pages/CheesyConsole";
import "./App.css";

function onAuthRequired({ history }) {
  history.push("/login");
}

class App extends Component {
  render() {
    return (
      <Router>
        <Security issuer="https://dev-717299.okta.com/oauth2/default"
          client_id="0oa13gp1e06DOFU55357"
          redirect_uri={window.location.origin + "/implicit/callback"}
          onAuthRequired={onAuthRequired}>
          <div className="App">
            <Navbar/>
            <div className="container">
              <Route path="/" exact component={Home}/>
              <SecureRoute path="/console" exact component={CheesyConsole}/>
              <Route path="/login" render={() => (<Login baseUrl="https://dev-717299.okta.com"/>)} />
              <Route path="/implicit/callback" component={ImplicitCallback}/>
            </div>
          </div>
        </Security>
      </Router>
    );
  }
}

export default App;
